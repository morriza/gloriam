const HtmlWebpackPlugin = require("html-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CompressionPlugin = require('compression-webpack-plugin');
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const ImageminPlugin = require('imagemin-webpack-plugin').default;
const imageminMozjpeg = require('imagemin-mozjpeg');
const webpack = require("webpack");
const path = require("path");
const globSync = require("glob").sync;
const modernizr = require("modernizr");

module.exports = (env, options) => ({
  entry: ["./src/index.js"],
  devServer: {
    contentBase: "./dist"
  },
  devtool: "source-map",
  node: {
    fs: 'empty'
  },
  module: {
    rules: [
      {
        test: /\.modernizrrc$/,
        loader: 'modernizr-loader!json-loader',
      },
      {
        test: /\.scss$/,
        use: [
          options.mode !== "production"
            ? "style-loader"
            // : MiniCssExtractPlugin.loader,
            : {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '../',
              }
            },
          "css-loader",
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('precss'),
                  require('autoprefixer')
                ];
              }
            }
          },
          "sass-loader"
        ]
      },
      {
        test: /\.(jpe?g|png|gif|svg|webp)$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "img/"
            }
          }
        ]
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
        use: [
          {
            loader: "file-loader",
            options: {
              name: "[name].[ext]",
              outputPath: "fonts/"
            }
          }
        ]
      },
      // {
      //   test: /\.(html)$/,
      //   use: {
      //     loader: "html-srcsets-loader",
      //     options: {
      //       attrs: [":src", ':srcset']
      //     }
      //   }
      // },
      {
        test: /\.(html)$/,
        use: {
          loader: "html-loader",
          options: {
            attrs: [":src", ':srcset'],
            interpolate: true,
            minimize: false,
            removeComments: true
          }
        }
      },
      {
        test: /\.js$/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"]
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      modernizr$: path.resolve(__dirname, '.modernizrrc'),
    }
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: "css/[name].[contenthash].css"
    }),
    new CleanWebpackPlugin(["dist"]),
    ...globSync("src/**/*.html").map(fileName => {
      return new HtmlWebpackPlugin({
        template: fileName,
        inject: "body",
        filename: fileName.replace("src/", "")
      });
    }),
    // new ImageminPlugin({
    //   disable: options.mode !== 'production', // Disable during development
    //   cacheFolder: path.resolve(__dirname, "cache"), // use existing folder called cache in the current dir
    //   pngquant: ({quality: '50-60'}),
    //   maxFileSize: 10000, // Only apply this one to files equal to or under 10kb
    //   plugins: [imageminMozjpeg({quality: 50, progressive: false})]
    // }),
    new ImageminPlugin({
      disable: options.mode !== 'production', // Disable during development
      cacheFolder: path.resolve(__dirname, "cache"), // use existing folder called cache in the current dir
      pngquant: ({quality: '50-60'}),
      minFileSize: 10000, // Only apply this one to files over 10kb
      plugins: [imageminMozjpeg({quality: 50, progressive: true})]
    }),
    new webpack.ProvidePlugin({
      $: "jquery",
      jQuery: "jquery",
      "window.jQuery": "jquery",
      Popper: ["popper.js", "default"],
      Util: "exports-loader?Util!bootstrap/js/dist/util",
      Dropdown: "exports-loader?Dropdown!bootstrap/js/dist/dropdown"
    })
  ],
  optimization: {
    minimizer: [
        new UglifyJsPlugin({
            cache: true,
            parallel: true,
            sourceMap: false,
            extractComments: false
        }),
        new CompressionPlugin({
            test: /\.js$|\.css(\?.*)?$/i
        }),
        new OptimizeCSSAssetsPlugin({})
    ]
  },
  output: {
    filename: "js/[name].[chunkhash].js",
    path: path.resolve(__dirname, "dist"),
    publicPath: ""
  }
});
