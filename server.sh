ADDR_PORT=${1:-127.0.0.1:8080}
DOC_ROOT=${2:-.}

php -S "$ADDR_PORT" -t "$DOC_ROOT/dist/"