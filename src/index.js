import './sass/main.scss';

import Modernizr from 'modernizr';

// require('bootstrap');
require('./js/bootstrap.js');
require('./js/jquery.scrollbar.js');
require('slick-carousel');

$(function() {

  $(window).scroll(function() {});

  $(window).on('resize', function() {});

  if (jQuery().slick) {

    $('#logo-slider').slick({
      dots: false,
      arrows: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 5,
      slidesToScroll: 1,
      responsive: [{
             breakpoint: 1024,
             settings: {
               slidesToShow: 4,
            }
          },
          {
            breakpoint: 600,
            settings: {
              slidesToShow: 3,
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 2,
              dots: true,
              arrows: false,
            }
          }
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
    });

    // $('.slide')
    //   .slick({
    //     dots: true,
    //     arrows: true,
    //     infinite: true,
    //     autoplay: true,
    //     autoplaySpeed: 10000,
    //     // speed: 500,
    //     slidesToShow: 1,
    //     slidesToScroll: 1,
    //     // fade: true,
    //     // cssEase: 'linear',
    //     responsive: [{
    //         breakpoint: 1024,
    //         settings: {
    //           slidesToShow: 1,
    //           slidesToScroll: 1,
    //           infinite: true,
    //           dots: true
    //         }
    //       },
    //       {
    //         breakpoint: 600,
    //         settings: {
    //           slidesToShow: 1,
    //           slidesToScroll: 1,
    //           dots: false
    //         }
    //       },
    //       {
    //         breakpoint: 480,
    //         settings: {
    //           slidesToShow: 1,
    //           slidesToScroll: 1,
    //           dots: false
    //         }
    //       }
    //       // You can unslick at a given breakpoint now by adding:
    //       // settings: "unslick"
    //       // instead of a settings object
    //     ]
    //   });

  }

});
